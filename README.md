# voltdb-latencyspikeawareclient

This package demonstrates how you can combine calls to "@ Statistics EXPORT" and
ClientConfig.setMaxTransactionsPerSecond() to have access to VoltDB throttled up 
or down based on a target latency measured in microseconds.

## How it works

Internally VoltDB tracks latency to the following percentiles:

* 50%
* 95% 
* 99%
* 99.9%
* 99.99%
* 99.999% 

Calling "@ Statistics EXPORT" returns a row for each node in the cluster:

````
TIMESTAMP      HOST_ID  HOSTNAME    INTERVAL  COUNT  TPS    P50  P95  P99  P99.9  P99.99  P99.999  MAX  
-------------- -------- ----------- --------- ------ ------ ---- ---- ---- ------ ------- -------- -----
 1558607746722        0 hardtofind       5000  84813  16962  177  357  472   2507    5971     6543  6607
````

In the example above 50% of transactions completed in 177 microseconds or under, 95% in 357 microseconds or under and so on.

The LatencyAwareClient is a wrapper class that periodically checks latency against a target defined when you instantiate it. If the 
latency is too high it reduces the maximum throughput it permits until it's acceptable. Conversely it will gradually increase 
throughout if latency is low. 

The client can pick any of the latencies, but we'd recommend P99 to start. 

The client is created using this constructor:

~~~~
  /**
     * Creates a client using the parameters below.
     * 
     * @param baseConfig
     *            ClientConfig as per usual...
     * @param hostnames
     *            comma delimited host names.
     * @param latencySampleSize
     *            How many data points we keep to calculate average latency. 5
     *            is a good start.
     * @param latencySampleIntervalMinutes.
     *            One is a good starting point.
     * @param tpsChangeRatePerIteration.
     *            1000 is a good starting point.
     * @param tps.
     *            How many TPS we allow at at start. Changes by
     *            m_tpsChangeRatePerIteration as we head towards target latency.
     * @param latencyTargetMicroSeconds.
     *            Note *microseconds*.
     * @param percentile. One of LEGAL_PERCENTILES (see above). e.g. 'P99'
     */
    public LatencyAwareClient(ClientConfig baseConfig, String[] hostnames, int latencySampleSize,
            int latencySampleIntervalMinutes, int tpsChangeRatePerIteration, int tps, int latencyTargetMicroSeconds, String percentile) 
~~~~
 
You then get the client each time you need it - the act of calling getClient() is what triggers the change in throughput limits

~~~~
 /**
     * Get client object. Call this every time you want to use the client, as
     * changing max TPS only happens in this call.
     * 
     * @return m_client
     * @throws ProcCallException
     * @throws IOException
     * @throws NoConnectionsException
     * @throws InterruptedException
     */
    public Client getClient() throws NoConnectionsException, IOException, ProcCallException, InterruptedException 
~~~~    

Note that many factors can influence latency, and that using this code might or might not help you. The intended use case is for when people have a production
 VoltDB system with sensitive SLAs but also have extra work they want to do without trashing the system's latency.
 
 We would recommend that if using this you start from a small tps value and allow activity to drift up slowly. We would advise against creating a large number of instances of this client, as while the call to @ Statistics is cheap, it isn't free...
 
A sample test run with the parameters '192.168.0.20 80 1  10000 P99 2000' looks like this:
 
![test run](https://bitbucket.org/sr_mad_science/voltdb-latencyspikeawareclient/raw/2afd8debcc7ea0a098513941323ba0f73ecdb96e/sample_run.png)
 
Note the saw tooth pattern for throughput as the Client tries to stay near the target latency.

