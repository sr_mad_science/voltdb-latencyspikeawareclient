package org.voltdb.latencyawareclient.test;

/* This file is part of VoltDB.
 * Copyright (C) 2008-2019 VoltDB Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;


import org.voltdb.client.ClientConfig;

import org.voltdb.client.ProcCallException;
import org.voltdb.latencyawareclient.LatencyAwareClient;

public class LatencyAwareClientTest {

    public static void main(String[] args) {

        final String dropTable = "DROP TABLE FOO IF EXISTS; ";
        final String createTable = "CREATE TABLE FOO (FOOCOL BIGINT NOT NULL PRIMARY KEY); ";
        final String partitionTable = "PARTITION TABLE FOO on COLUMN FOOCOL;";

        String hostname = args[0];
        String[] hostnameArray = hostname.split(",");
        int busyClientTps = Integer.parseInt(args[1]);
        int deltaTps = Integer.parseInt(args[2]);
        int runDurationSecs = Integer.parseInt(args[3]);
        String percentile = args[4];
        int latencyTargetMicroSeconds = Integer.parseInt(args[5]);

        ClientConfig testClientConfig = new ClientConfig();
        testClientConfig.setMaxOutstandingTxns(200000);
        testClientConfig.setMaxTransactionsPerSecond(1000000);

        int latencySampleSize = 5;
        int latencySampleIntervalMinutes = 1;
        int tpsChangeRatePerIteration = deltaTps;

     
        LatencyAwareClient lac = new LatencyAwareClient(testClientConfig, hostnameArray, latencySampleSize,
                latencySampleIntervalMinutes, tpsChangeRatePerIteration, busyClientTps, latencyTargetMicroSeconds,
                percentile);

        try {
            lac.getClient().callProcedure("@AdHoc", dropTable);
            lac.getClient().callProcedure("@AdHoc", createTable);
            lac.getClient().callProcedure("@AdHoc", partitionTable);

            long endTimeMS = System.currentTimeMillis() + (runDurationSecs * 1000);
            long lastReportTime = 0;
            long recordCount = 0;
            long lastReportedRecordCount = 0;

            ComplainOnErrorCallback coec = new ComplainOnErrorCallback();

            while (System.currentTimeMillis() < endTimeMS) {

                lac.getClient().callProcedure(coec, "foo.delete", (recordCount - 5000000));
                lac.getClient().callProcedure(coec, "foo.insert", recordCount);
                recordCount += 2;

                if (lastReportTime + (10 * 1000) < System.currentTimeMillis()) {
                    lastReportTime = System.currentTimeMillis();
                    System.out.println("TPMS=" + ((recordCount - lastReportedRecordCount) / 10000) + " : " + lac.toString() + ":"
                            + recordCount + ":" + lastReportedRecordCount);
                    lastReportedRecordCount = recordCount;
                }
            }

            lac.getClient().callProcedure("@AdHoc", dropTable);
            lac.close();
            
        } catch (IOException | ProcCallException | InterruptedException e) {
            e.printStackTrace();
        }

    }

}
