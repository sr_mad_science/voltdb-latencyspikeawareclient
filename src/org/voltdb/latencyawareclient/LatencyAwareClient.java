package org.voltdb.latencyawareclient;

/* This file is part of VoltDB.
 * Copyright (C) 2008-2019 VoltDB Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

import java.io.IOException;
import java.net.UnknownHostException;

import org.voltdb.client.Client;
import org.voltdb.client.ClientConfig;
import org.voltdb.client.ClientFactory;
import org.voltdb.client.ClientResponse;
import org.voltdb.client.NoConnectionsException;
import org.voltdb.client.ProcCallException;

/**
 * Simple client wrapper for scenarios where we are running a secondary workload
 * and don't want to harm latency in the main workload.
 * 
 * We do this by measuring the rolling average 99.99% latency every few minutes
 * and nudging our max TPS up or down as appropriate.
 * 
 * Note that the act of measuring the latency isn't free, so we don't do it that
 * often.
 * 
 * @author drolfe@voltdb.com
 *
 */
public class LatencyAwareClient {

    public static final String[] LEGAL_PERCENTILES = { "P50", "P95", "P99", "P99.9", "P99.99", "P99.999" };

    /**
     * Client Config containing DB info. Note that we call
     * setMaxTransactionsPerSecond and re-use this every time we recycle a
     * client. We also assume that users call 'getClient()' each and every time
     * they make a call to VoltDB - the process to change max TPS is tied to it.
     */
    ClientConfig m_baseConfig = null;

    /**
     * How many latency samples we keep for calculating averages
     */
    int m_latencySampleSize = 100;

    /**
     * How many minutes we wait between samples
     */
    int m_latencySampleIntervalMinutes = 1;

    /**
     * How quickly we change the max TPS rate.
     */
    int m_tpsChangeRatePerIteration = 1000;

    /**
     * Current TPS rate per ms.
     */
    int m_tpms = Integer.MIN_VALUE;

    /**
     * How bad latency can be before we reduce the workload.
     */
    int m_latencyTargetMicroSeconds = 2000;

    /**
     * Our VoltDB client. Note that we re-instantiate it every time we change
     * our mind about max TPS.
     */
    Client m_client = null;

    /**
     * Last unixtime we checked latency
     */
    long m_lastLatencyCheckTime = 0;

    /**
     * Store of latency reports
     */
    long[] m_latencyReports = null;

    /**
     * How many latency reports we've had.
     */
    int latencyReportCount = 0;

    /**
     * List of target host names.
     */
    private String[] m_hostnames;

    /**
     * Debug mode flag
     */
    private boolean m_debug = true;
    
    /**
     * A valid value of LEGAL_PERCENTILES
     */
    String m_percentile =null;

    /**
     * Creates a client using the parameters below.
     * 
     * @param baseConfig
     *            ClientConfig as per usual...
     * @param hostnames
     *            comma delimited host names.
     * @param latencySampleSize
     *            How many data points we keep to calculate average latency. 5 is a good start.
     * @param latencySampleIntervalMinutes.
     *            One is a good starting point.
     * @param tpsChangeRatePerIteration.
     *            1000 is a good starting point.
     * @param tps.
     *            How many TPS we allow at at start. Changes by
     *            m_tpsChangeRatePerIteration as we head towards target latency.
     * @param latencyTargetMicroSeconds.
     *            Note *microseconds*.
     * @param percentile. One of LEGAL_PERCENTILES (see above). e.g. 'P99'
     */
    public LatencyAwareClient(ClientConfig baseConfig, String[] hostnames, int latencySampleSize,
            int latencySampleIntervalMinutes, int tpsChangeRatePerIteration, int tps, int latencyTargetMicroSeconds, String percentile) {
        super();
        
        assert latencySampleIntervalMinutes > 0 : "Sample Interval must be >= 1";
        
        this.m_baseConfig = baseConfig;
        this.m_hostnames = hostnames;
        this.m_latencySampleSize = latencySampleSize;
        this.m_latencySampleIntervalMinutes = latencySampleIntervalMinutes;
        this.m_tpsChangeRatePerIteration = tpsChangeRatePerIteration;
        this.m_tpms = tps;
        m_baseConfig.setMaxTransactionsPerSecond(m_tpms * 1000);
        this.m_latencyTargetMicroSeconds = latencyTargetMicroSeconds;
        
        for (int i=0; i < LEGAL_PERCENTILES.length; i++) {
            if (percentile.equalsIgnoreCase(LEGAL_PERCENTILES[i])) {
                this.m_percentile = LEGAL_PERCENTILES[i];
                break;
            }
        }
        
        if (this.m_percentile == null) {
            this.m_percentile = LEGAL_PERCENTILES[2];  
        }
        

    }

    /**
     * Get client object. Call this every time you want to use the client, as
     * changing max TPS only happens in this call.
     * 
     * @return m_client
     * @throws ProcCallException
     * @throws IOException
     * @throws NoConnectionsException
     * @throws InterruptedException
     */
    public Client getClient() throws NoConnectionsException, IOException, ProcCallException, InterruptedException {

        if (m_client == null) {
            connect();
            targetTpsHasChanged(m_latencyTargetMicroSeconds);
        } else if (m_lastLatencyCheckTime + (m_latencySampleIntervalMinutes * 60 * 1000) < System.currentTimeMillis()) {

            synchronized (m_client) {
                m_lastLatencyCheckTime = System.currentTimeMillis();

                // Call VoltDB procedure to get latency stats.
                ClientResponse statsResponse = m_client.callProcedure("@Statistics", "LATENCY", 0);

                if (m_debug) {
                    System.out.println(statsResponse.getResults()[0].toFormattedString());
                }

                // get average latency as of now...
                long latency = 0;
                while (statsResponse.getResults()[0].advanceRow()) {
                    latency += statsResponse.getResults()[0].getLong(m_percentile);
                }
                latency /= statsResponse.getResults()[0].getRowCount();

                // We have to re-create the client to change the TPS limit...
                if (targetTpsHasChanged(latency)) {
                    connect();
                }
            }
        }

        return m_client;
    }

    /**
     * Disconnect and re-connect to VoltDB
     * 
     * @throws IOException
     * @throws UnknownHostException
     */
    private void connect() throws UnknownHostException, IOException {

        if (m_client != null) {

            try {
                m_client.drain();
                m_client.close();
            } catch (NoConnectionsException | InterruptedException e) {
                e.printStackTrace();
            }
            m_client = null;

        }
        m_client = ClientFactory.createClient(m_baseConfig);

        for (int i = 0; i < m_hostnames.length; i++) {

            m_client.createConnection(m_hostnames[i]);

        }

    }

    /**
     * Update latency stats and decide if a change is needed. We 
     * reuse the array positions on a 'round robin' basis.
     * 
     * @param latestLacency
     *            reported in microseconds
     * @return true if target has changed.
     */
    private boolean targetTpsHasChanged(long latestLacency) {

        latencyReportCount++;

        // Simple case: First time we're called.
        if (m_latencyReports == null) {
            m_latencyReports = new long[m_latencySampleSize];
            for (int i = 0; i < m_latencyReports.length; i++) {
                m_latencyReports[i] = latestLacency;
            }

            return false;

        }

        // Update oldest member of stats array...
        m_latencyReports[latencyReportCount % m_latencySampleSize] = latestLacency;

        // Calculate average
        long latencyAverage = calculateAverage();

        // if average is higher than we'd like reduce target TPS...
        if (latencyAverage > m_latencyTargetMicroSeconds && m_tpms > m_tpsChangeRatePerIteration) {
            m_tpms -= m_tpsChangeRatePerIteration;
            m_baseConfig.setMaxTransactionsPerSecond(m_tpms * 1000);

            if (m_debug) {
                System.out.println("DOWN:" + toString());
            }
            return true;
        }

        // if average and last reading are both lower then we can increase target TPS...
        if (latencyAverage < m_latencyTargetMicroSeconds && latestLacency < m_latencyTargetMicroSeconds) {
            m_tpms += m_tpsChangeRatePerIteration;
            m_baseConfig.setMaxTransactionsPerSecond(m_tpms * 1000);
            if (m_debug) {
                System.out.println("UP  :" + toString());
            }
            return true;
        }

        return false;
    }

    /**
     * Calculate average of latency
     * @param latencyAverage
     * @return
     */
    private long calculateAverage() {

        if (m_latencyReports == null) {
            return Long.MIN_VALUE;
        }

        long latencyAverage = 0;

        for (int i = 0; i < m_latencyReports.length; i++) {
            latencyAverage += m_latencyReports[i];
        }

        latencyAverage /= m_latencyReports.length;
        return latencyAverage;
    }

    /**
     * Close client.
     */
    public void close() {
        synchronized (m_client) {

            try {
                m_client.drain();
                m_client.close();
            } catch (NoConnectionsException | InterruptedException e) {
                e.printStackTrace();
            }
            m_client = null;
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {

        StringBuffer sb = new StringBuffer("TPMS/Target Latency/Average Latency/Percentile/Data=");

        sb.append(m_tpms);
        sb.append('/');

        sb.append(m_latencyTargetMicroSeconds);
        sb.append('/');

        sb.append(calculateAverage());
        sb.append('/');

        sb.append(m_percentile);
        sb.append('/');

        if (m_latencyReports != null) {
            for (int i = 0; i < m_latencyReports.length; i++) {
                if (i > 0) {
                    sb.append(',');
                }
                sb.append(m_latencyReports[i]);

            }

        }

        return sb.toString();
    }

}
